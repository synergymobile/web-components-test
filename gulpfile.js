/**
 * Created by Nemanja Zoric on 14/04/2021
 */
const gulp = require('gulp');
const inject = require('gulp-inject');

gulp.task('index', () => {
    const target = gulp.src('./index.html');
    const sources = gulp.src(['node_modules/wc-card-element/card-element.js', './node_modules/wc-card-element/card-element.css'], {read: false});

    return target.pipe(inject(sources, {addRootSlash: false})).pipe(gulp.dest('./'));
})
