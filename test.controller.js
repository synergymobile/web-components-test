/**
 * Created by Nemanja Zoric on 31/03/2021
 */
(function () {
    'use strict';

    angular
        .module('test-component')
        .controller('TestCtrl', TestCtrl);

    function TestCtrl() {
        const init = function () {
            const el = document.querySelector('synergy-card');
            el.addEventListener('likeNotify', e => {
                console.log('Like Clicked', e.detail);
            });
            el.addEventListener('shareNotify', e => {
                console.log('Share Clicked', e.detail);
            });
            el.addEventListener('commentNotify', e => {
                console.log('Comment Clicked', e.detail);
            });
        }

        init();
    }

})();
