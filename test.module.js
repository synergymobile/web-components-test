/**
 * Created by Nemanja Zoric on 31/03/2021
 */
(function () {
    'use strict';

    angular
        .module('test-component', []);
})();
